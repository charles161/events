import React from 'react';
import {
  View
} from 'react-native';

const Card = (props) => {
  return (
    <View style={styles.containerStyle}>
    {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderRadius: 7,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    elevation: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 13
  }
};

export { Card };
