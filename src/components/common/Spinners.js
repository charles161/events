import React from 'react';
import { View } from 'react-native';
import { Spinner } from 'nachos-ui';


const Spinners = () => {
  return (
    <View style={styles.spinnerstyle}>
      <Spinner />
    </View>
  );
};

const styles = {
  spinnerstyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export { Spinners };
