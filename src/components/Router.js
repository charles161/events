import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Events from './Events';


const RouterComponent = () => {
  return (
    <Router >
      <Scene>
        <Scene key='events' component={Events} title='Events' initial />
      </Scene>
    </Router>
  );
};

export default RouterComponent;
