import React, { Component } from 'react';
import { View, ScrollView, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
 import { Actions } from 'react-native-router-flux';
import { getQuery, getRequest } from '../actions';
import { OATTENDANCE,
  ATTENDANCE,
   INTERNALS,
   NOTIFICATIONS,
   WALLETDETAILS,
   EVENTS,
   SLIDES,
   CLUBS_LIST,
   MESSMENU,
  COMMONQUERY_FAIL } from '../actions/types';
import { Spinners, Button } from './common';
import ListItem from './ListItem';

class Events extends Component {

  buttonPress() {
    Actions.overallattendance();
  }

  renderRow(library) {
    return <ListItem library={library} />;
  }

  renderView() {
    console.log(this.props.events);
    console.log(this.props.transactions);
    console.log(this.props.wallet_details);
    console.log(this.props.internals);
    console.log(this.props.attendance);
    console.log(this.props.percent);
    console.log(this.props.slides);
    console.log(this.props.clubslist);
    console.log(this.props.messmenu);
    if (this.props.loading) {
      this.props.getRequest(EVENTS);
      this.props.getRequest(SLIDES);
      this.props.getRequest(CLUBS_LIST);
      this.props.getRequest(MESSMENU);
      this.props.getQuery(NOTIFICATIONS, this.props.roll);
      this.props.getQuery(OATTENDANCE, this.props.roll);
      this.props.getQuery(WALLETDETAILS, this.props.roll);
      this.props.getQuery(ATTENDANCE, this.props.roll);
      this.props.getQuery(INTERNALS, this.props.roll);
      return <Spinners />;
    }
    return (
      <View>
        <Button>
          Get Overall Attendance
        </Button>
      </View>
    );
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>

        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>

          {this.renderView()}

      </ScrollView>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 10,
    alignSelf: 'center',
    color: 'red'
  }
};

const mapStateToProps = ({commonRequestReducer, RollReducer }) => {
  const {
     error,
loading,
percent,
lastUpdated,
attendance,
internals,
notifications,
wallet_details,
transactions,
events,
slides,
clubslist,
messmenu } = commonRequestReducer;
const { roll } = RollReducer;
  return {
    error,
loading,
percent,
lastUpdated,
attendance,
internals,
notifications,
wallet_details,
transactions,
events,
slides,
clubslist,
messmenu,
roll
  };
};


export default connect(mapStateToProps, { getRequest, getQuery })(Events);
