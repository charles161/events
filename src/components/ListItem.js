import React, { Component } from 'react';
import {
  Text,
  View,
  LayoutAnimation,
  Image,
 TouchableHighlight
 } from 'react-native';//* as components from
 import { Actions } from 'react-native-router-flux';
import { Card, CardSection } from './common';

class ListItem extends Component {

  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  onItemPress() {
    Actions.event({ event: this.props.library.item });
  }

  render() {
    const { textcontainer, titleheader, imageStyle } = styles;
    const {
      title,
      short_description,
      feature_image,
      happening_at
    } = this.props.library.item;
    const dat = new Date(happening_at);

    return (
      <TouchableHighlight onPress={this.onItemPress.bind(this)}>
      <View>
        <Card>
          <CardSection>
            <Image
            style={imageStyle}
            source={{ uri: 'http://katchup.iqube.io' + feature_image }}
            />
          </CardSection>

          <CardSection>
             <View style={textcontainer}>
             <Text style={titleheader}>{title}</Text>
             </View>
          </CardSection>

          <CardSection>
            <Text>{short_description}</Text>
          </CardSection>


          <CardSection>
             <Text>Date: {dat.toUTCString()}</Text>
          </CardSection>

        </Card>

        </View>

      </TouchableHighlight>
    );
  }
}

const styles = {
  textcontainer: {
  justifyContent: 'space-around',
  flexDirection: 'column'
},
  titleheader: {
    fontSize: 18,
    fontWeight: '500'
  },
  imageStyle: {
    height: 162,
    flex: 1,
    width: 0
  }
};

export default ListItem;
