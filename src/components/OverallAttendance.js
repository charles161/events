import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { getoverallattendance } from '../actions';
import { Spinners, Button } from './common';


class OverallAttendance extends Component {

  renderView() {
    if (this.props.loading) {
      this.props.getoverallattendance(this.props.roll);
      return <Spinners />;
    }
    return (
      <Button>
        <Text>{this.props.percent}</Text>
      </Button>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>
          {this.renderView()}
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 10,
    alignSelf: 'center',
    color: 'red'
  }
};

const mapStateToProps = ({ OverallAttenReducer, RollReducer }) => {
  const { error, loading, percent, lastUpdated } = OverallAttenReducer;
  const { roll } = RollReducer;
  return {
    error,
    loading,
    percent,
    lastUpdated,
    roll
  };
};


export default connect(mapStateToProps, { getoverallattendance })(OverallAttendance);
