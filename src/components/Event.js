import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  LayoutAnimation,
  Image, Linking
 } from 'react-native';//* as components from
import { Card, CardSection, Button } from './common';

class Event extends Component {

  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  render() {
    const { textcontainer, titleheader, imageStyle, textdescription } = styles;
    const {
      title,
      description,
      feature_image,
      registration_link,
      happening_at
    } = this.props.event;
    const dat = new Date(happening_at);

    return (
        <Card>
        <ScrollView>
          <CardSection>
             <View style={textcontainer}>
             <Text style={titleheader}>{title}</Text>
             </View>
          </CardSection>

          <CardSection>
            <Image
            style={imageStyle}
            source={{ uri: 'http://katchup.iqube.io'+feature_image }}
            />
          </CardSection>

          <CardSection>
            <Text style={textdescription}>{description}</Text>
          </CardSection>

          <CardSection>
             <Text > Date: {dat.toUTCString()}</Text>
          </CardSection>

          <CardSection>
            <Button onPress={() => Linking.openURL(registration_link)} >
               Register
            </Button>
          </CardSection>
          </ScrollView>
        </Card>

    );
  }
}

const styles = {
  textcontainer: {
  justifyContent: 'space-around',
  flexDirection: 'column'
},
  titleheader: {
    fontSize: 18,
    fontWeight: '500'
  },
  textdescription: {
    fontSize: 13,
    alignSelf: 'center'
  },
  imageStyle: {
    height: 162,
    flex: 1,
    width: null
  }
};

export default Event;
