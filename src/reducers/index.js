import { combineReducers } from 'redux';
import commonRequestReducer from './commonRequestReducer';
import RollReducer from './RollReducer';

export default combineReducers({
    commonRequestReducer,
    RollReducer
});
