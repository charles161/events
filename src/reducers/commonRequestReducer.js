import { OATTENDANCE,
  ATTENDANCE,
   INTERNALS,
   NOTIFICATIONS,
   WALLETDETAILS,
   EVENTS,
   SLIDES,
   CLUBS_LIST,
   MESSMENU,
  COMMONQUERY_FAIL } from '../actions/types';

const INITIAL_STATE = {
   error: '',
 loading: true,
percent: '',
lastUpdated: '',
attendance: [],
internals: [],
notifications: [],
wallet_details: {},
transactions: [],
events: [],
slides: [],
clubslist: [],
messmenu: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case OATTENDANCE:
      return { ...state,
         percent: action.payload.percent,
          lastUpdated: action.payload.updated_date,
           loading: false };
    case ATTENDANCE:
      return { ...state,
              attendance: action.payload,
              loading: false };
    case INTERNALS:
      return { ...state,
              internals: action.payload,
              loading: false };
    case NOTIFICATIONS:
      return { ...state,
              notifications: action.payload,
              loading: false };
    case WALLETDETAILS:
      return { ...state,
              wallet_details: action.payload,
              transactions: action.payload.transactions,
             loading: false };
    case EVENTS:
      return { ...state,
            events: action.payload,
            loading: false };
    case SLIDES:
      return { ...state,
            slides: action.payload,
            loading: false };
    case CLUBS_LIST:
      return { ...state,
            clubslist: action.payload,
            loading: false };
    case MESSMENU:
      return { ...state,
            messmenu: action.payload,
            loading: false };
    case COMMONQUERY_FAIL:
        return { ...state,
          percent: '',
           lastUpdated: '',
           error: 'Server Error',
             loading: false };
    default:
      return state;
  }
};
