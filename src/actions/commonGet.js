import axios from 'axios';
import { COMMONQUERY_FAIL } from './types';

export const getRequest = (query) => {
  return (dispatch) => {
    axios.get('https://katchup.iqube.io/api/v1/' + query)
      .then(response =>
        ProcessSuccess(dispatch, response))
      .catch(() => Processfail(dispatch));
  };
};

const Processfail = (dispatch) => {
  dispatch({ type: COMMONQUERY_FAIL });
};

const ProcessSuccess = (dispatch, response, query) => {
  dispatch({
    type: query,
     payload: response.data
   });
};
