import axios from 'axios';
import { COMMONQUERY_FAIL } from './types';

export const getQuery = (query, roll) => {
  return (dispatch) => {
    axios.get('https://katchup.iqube.io/api/v1/' + query + '/?roll_no=' + roll)
      .then(response =>
        ProcessSuccess(dispatch, response))
      .catch(() => Processfail(dispatch));
  };
};

const Processfail = (dispatch) => {
  dispatch({ type: COMMONQUERY_FAIL });
};

const ProcessSuccess = (dispatch, response, query) => {
  dispatch({
    type: query,
     payload: response.data
   });
};
