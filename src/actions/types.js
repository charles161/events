export const ROLL = 'roll_no';

export const COMMONQUERY_FAIL = 'common_query_fail';

export const INTERNALS = 'internals/';
export const OATTENDANCE = 'overall_attendance/';
export const ATTENDANCE = 'attendance/';
export const WALLETDETAILS = 'get_wallet_details';
export const NOTIFICATIONS = 'notifications/';

export const EVENTS = 'events/';
export const SLIDES = 'slides/';
export const CLUBS_LIST = 'clubs_list';
export const MESSMENU = 'mess_menu_list';
